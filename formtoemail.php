<!doctype html>
<title>Thanks for the email | Tabbuli Charleston, SC</title>
<link href="css/package-styles.css" rel="stylesheet" type="text/css">
<?php

$my_email = "chgforms@gmail.com";

/*

Enter the continue link to offer the user after the form is sent.  If you do not change this, your visitor will be given a continue link to your homepage.

If you do change it, remove the "/" symbol below and replace with the name of the page to link to, eg: "mypage.htm" or "http://www.elsewhere.com/page.htm"

*/

$continue = "http://tabbuli.com";

/*

Step 3:

Save this file (FormToEmail.php) and upload it together with your webpage containing the form to your webspace.  IMPORTANT - The file name is case sensitive!  You must save it exactly as it is named above!  Do not put this script in your cgi-bin directory (folder) it may not work from there.

THAT'S IT, FINISHED!

You do not need to make any changes below this line.

*/

$errors = array();

// Remove $_COOKIE elements from $_REQUEST.

if(count($_COOKIE)){foreach(array_keys($_COOKIE) as $value){unset($_REQUEST[$value]);}}

// Check all fields for an email header.

function recursive_array_check_header($element_value)
{

global $set;

if(!is_array($element_value)){if(preg_match("/(%0A|%0D|\n+|\r+)(content-type:|to:|cc:|bcc:)/i",$element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_header($value);}

}

}

recursive_array_check_header($_REQUEST);

if($set){$errors[] = "You cannot send an email header";}

unset($set);

// Validate email field.

if(isset($_REQUEST['email']) && !empty($_REQUEST['email']))
{

if(preg_match("/(%0A|%0D|\n+|\r+|:)/i",$_REQUEST['email'])){$errors[] = "Email address may not contain a new line or a colon";}

$_REQUEST['email'] = trim($_REQUEST['email']);

if(substr_count($_REQUEST['email'],"@") != 1 || stristr($_REQUEST['email']," ")){$errors[] = "Email address is invalid";}else{$exploded_email = explode("@",$_REQUEST['email']);if(empty($exploded_email[0]) || strlen($exploded_email[0]) > 64 || empty($exploded_email[1])){$errors[] = "Email address is invalid";}else{if(substr_count($exploded_email[1],".") == 0){$errors[] = "Email address is invalid";}else{$exploded_domain = explode(".",$exploded_email[1]);if(in_array("",$exploded_domain)){$errors[] = "Email address is invalid";}else{foreach($exploded_domain as $value){if(strlen($value) > 63 || !preg_match('/^[a-z0-9-]+$/i',$value)){$errors[] = "Email address is invalid"; break;}}}}}}

}

// Check referrer is from same site.

if(!(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'],$_SERVER['HTTP_HOST']))){$errors[] = "You must enable referrer logging to use the form";}

// Check for a blank form.

function recursive_array_check_blank($element_value)
{

global $set;

if(!is_array($element_value)){if(!empty($element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_blank($value);}

}

}

recursive_array_check_blank($_REQUEST);

if(!$set){$errors[] = "You cannot send a blank form";}

unset($set);

// Display any errors and exit if errors exist.

if(count($errors)){foreach($errors as $value){print "$value<br>";} exit;}

if(!defined("PHP_EOL")){define("PHP_EOL", strtoupper(substr(PHP_OS,0,3) == "WIN") ? "\r\n" : "\n");}

// Build message.

function build_message($request_input){if(!isset($message_output)){$message_output ="";}if(!is_array($request_input)){$message_output = $request_input;}else{foreach($request_input as $key => $value){if(!empty($value)){if(!is_numeric($key)){$message_output .= str_replace("_"," ",ucfirst($key)).": ".build_message($value).PHP_EOL.PHP_EOL;}else{$message_output .= build_message($value).", ";}}}}return rtrim($message_output,", ");}

$message = build_message($_REQUEST);

$message = $message . PHP_EOL.PHP_EOL."-- ".PHP_EOL."";

$message = stripslashes($message);

$subject = "tabbuli website inquiry";

$headers = "From: " . $_REQUEST['email'];

mail($my_email,$subject,$message,$headers);

?>

<!-- include your own success html here -->
<html><head>
<meta charset="UTF-8">
<title>Contact Us | Tabbuli</title>

<meta name="viewport" content="width=device-width, initial-scale=no"> 
        <meta name="google-site-verification" content="" />
        <meta name="description" content="We are located at 541 King Street in Charleston, SC &lowast; 843.628.5959" />
        <meta name="keywords" content="Charleston, Hookah, Outdoor, Outdoor Patio, Sunset Restaurant, King Street" />
        <meta name="author" content="Charleston Hospitality Group" />
        <meta name="robots" content="index, follow"/>
        <meta name="distribution" content="global"/>
        <link rel="shortcut icon" href="favicon.ico"> 
<link href="css/global.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Questrial|Coustard' rel='stylesheet' type='text/css'>
   <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24705264-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
   <script src="js/chownow.js"></script>
   
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

    <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '896758270739348');

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=896758270739348&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->

</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=382880455061904";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<div class="container">
  <div class="header">
       <nav class="content">
       <div class="header-contact">
       <a href="index.php" title="Home"><img src="images/logo.png" width="210" height="80" alt="Tabbuli"></a><br />
      <h4 class="white-text phone-spacing"><a class="phone" href="tel:8436285959">Downtown &bull; 843.628.5959</a><br />
      <a class="phone" href="tel:8435560006">West Ashley &bull; 843.556.0006</a>
      </h4>
       <div class="clearfloat"></div>
       </div>
 
<ul class="nav">
        <li class="nav"><a href="index.php" title="Home">home</a></li>
        <li class="nav"><a href="menu.html" title="Lunch and Dinner Menu">menu</a></li>
        <li class="nav"><a href="brunch.html" title="Brunch Menu">brunch menu</a></li>
        </ul>
        <ul class="nav">
        <li class="nav"><a href="gallery.html" title="Gallery">gallery</a></li>
         <li class="nav"><a href="https://charlestonhospitalitygroup.securetree.com/" title="Gift Cards">gift cards</a></li>
         <li class="nav"><a href="newsletter.html" title="Sign up for our Newsletter">newsletter sign up</a></li>
        </ul>
        <ul class="nav">
	    <li class="nav"><a href="https://recruiting.paylocity.com/recruiting/jobs/All/7c844750-f56f-4030-b990-3fece9783f51/CHG-LLC" title="Careers" target="_blank">careers</a></li>
        <li class="nav"><a href="events.html" title="Blog &amp; Events">vip events</a></li>
        <li class="nav"><a href="shop.html" title="Shop Online">shop</a></li>
        </ul>
        <ul class="nav">
	        <li class="nav"><a href="https://www.ubereats.com/charleston/food-delivery/queology-and-tabbuli/aDS5AaY0RfWdiIdj3OvDcw" title="Takeout and Delivery" target="_blank">takeout + delivery</a></li>
	        <li class="nav"><a href="contact.html" title="Contact us">location + contact</a></li>
	        <li class="nav"><a href="catering.html" title="Catering">catering</a></li>
</ul> 
       <!--end content--> </nav>
    <!-- end .header --></div>
     
  <div class="content">
  <div class="menu-spacing"></div>
  <div class="menu-spacing"></div>
 
 <h1>Thank you!</h1>
 <h2>We have received your message and will be in touch shortly.</h2>

 <!--end content--> </div>
 
 <div class="menu-spacing"></div>
 <div class="menu-spacing"></div>
  
  <div class="footer">
  <div class="content">
   <ul class="nav footer-nav">
        <li class="nav"><a href="index.php" title="Home">home</a></li>
        <li class="nav"><a href="menu.html" title="Lunch and Dinner Menu">lunch + dinner menu</a></li>
        <li class="nav"><a href="brunch.html" title="Brunch Menu">brunch menu</a></li>
        </ul>
        <ul class="nav footer-nav">
        <li class="nav"><a href="gallery.html" title="Gallery">gallery</a></li>
        <li class="nav"><a href="http://tabbuligrill.wordpress.com/" title="Blog &amp; Events" target="_blank">blog</a></li>
        <li class="nav"><a href="newsletter.html" title="Sign up for our Newsletter">newsletter sign up</a></li>
        </ul>
        <ul class="nav footer-nav">
	    <li class="nav"><a href="events.html" title="VIP Events">vip events</a></li>
		<li class="nav"><a href="https://charlestonhospitalitygroup.securetree.com/" title="Gift Cards">gift cards</a></li>
        <li class="nav"><a href="contact.html" title="Contact us">location + contact</a></li>
  </ul>
       
        <!-- Begin MailChimp Signup Form -->

<div class="inline form-padding">	
	<span class="form-label">Get Social with us</span> <br /><br />
<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="200" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
<br />
<a href="https://twitter.com/tabbuli" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @tabbuli</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>
<!-- Please call pinit.js only once per page -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<!--<div id="newsletter">
<form action="http://tabbuli.us6.list-manage2.com/subscribe/post?u=c752e994be9361b33f9f52d53&amp;id=e4880114ed" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="mc-field-group">
	<label for="mce-EMAIL"><span class="form-label">Sign up for our newsletter</span></label><br />
	<input type="email" value="" name="EMAIL" class="FieldsInput" id="mce-EMAIL" placeholder="Email Address" required>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response"></div>
		<div class="response" id="mce-success-response"></div>
	</div>	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="submit-button"></div>
</form>
<!--end newsletter--> <!-- </div> -->
<!--end inline--> </div>

<div class="inline form-padding">
	<div id="TA_certificateOfExcellence217" class="TA_certificateOfExcellence">
<ul id="sZmNO4pqRVAQ" class="TA_links Gapmtwnuw">
<li id="DKSyvth" class="e7hyBf">
<a target="_blank" href="http://www.tripadvisor.com/Restaurant_Review-g54171-d1903985-Reviews-Tabbuli_Grill_Mediterranean_American-Charleston_South_Carolina.html"><img src="http://www.tripadvisor.com/img/cdsi/img2/awards/CoE2015_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a>
</li>
</ul>
</div>
<script src="http://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=217&amp;locationId=1903985&amp;lang=en_US&amp;year=2015&amp;display_version=2"></script>

<!--end inline--> </div>

<script type="text/javascript">
var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
try {
    var jqueryLoaded=jQuery;
    jqueryLoaded=true;
} catch(err) {
    var jqueryLoaded=false;
}
var head= document.getElementsByTagName('head')[0];
if (!jqueryLoaded) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
    head.appendChild(script);
    if (script.readyState && script.onload!==null){
        script.onreadystatechange= function () {
              if (this.readyState == 'complete') mce_preload_check();
        }    
    }
}
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
head.appendChild(script);
var err_style = '';
try{
    err_style = mc_custom_error_style;
} catch(e){
    err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
}
var head= document.getElementsByTagName('head')[0];
var style= document.createElement('style');
style.type= 'text/css';
if (style.styleSheet) {
  style.styleSheet.cssText = err_style;
} else {
  style.appendChild(document.createTextNode(err_style));
}
head.appendChild(style);
setTimeout('mce_preload_check();', 250);

var mce_preload_checks = 0;
function mce_preload_check(){
    if (mce_preload_checks>40) return;
    mce_preload_checks++;
    try {
        var jqueryLoaded=jQuery;
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    try {
        var validatorLoaded=jQuery("#fake-form").validate({});
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    mce_init_form();
}
function mce_init_form(){
    jQuery(document).ready( function($) {
      var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
      var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
      $("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
      options = { url: 'http://tabbuli.us6.list-manage2.com/subscribe/post-json?u=c752e994be9361b33f9f52d53&id=e4880114ed&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
                    beforeSubmit: function(){
                        $('#mce_tmp_error_msg').remove();
                        $('.datefield','#mc_embed_signup').each(
                            function(){
                                var txt = 'filled';
                                var fields = new Array();
                                var i = 0;
                                $(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                                $(':hidden', this).each(
                                    function(){
                                        var bday = false;
                                        if (fields.length == 2){
                                            bday = true;
                                            fields[2] = {'value':1970};//trick birthdays into having years
                                        }
                                    	if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
                                    		this.value = '';
									    } else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
                                    		this.value = '';
									    } else {
									        if (/\[day\]/.test(fields[0].name)){
    	                                        this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;									        
									        } else {
    	                                        this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
	                                        }
	                                    }
                                    });
                            });
                        return mce_validator.form();
                    }, 
                    success: mce_success_cb
                };
      $('#mc-embedded-subscribe-form').ajaxForm(options);
      
      
    });
}
function mce_success_cb(resp){
    $('#mce-success-response').hide();
    $('#mce-error-response').hide();
    if (resp.result=="success"){
        $('#mce-'+resp.result+'-response').show();
        $('#mce-'+resp.result+'-response').html(resp.msg);
        $('#mc-embedded-subscribe-form').each(function(){
            this.reset();
    	});
    } else {
        var index = -1;
        var msg;
        try {
            var parts = resp.msg.split(' - ',2);
            if (parts[1]==undefined){
                msg = resp.msg;
            } else {
                i = parseInt(parts[0]);
                if (i.toString() == parts[0]){
                    index = parts[0];
                    msg = parts[1];
                } else {
                    index = -1;
                    msg = resp.msg;
                }
            }
        } catch(e){
            index = -1;
            msg = resp.msg;
        }
        try{
            if (index== -1){
                $('#mce-'+resp.result+'-response').show();
                $('#mce-'+resp.result+'-response').html(msg);            
            } else {
                err_id = 'mce_tmp_error_msg';
                html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
                
                var input_id = '#mc_embed_signup';
                var f = $(input_id);
                if (ftypes[index]=='address'){
                    input_id = '#mce-'+fnames[index]+'-addr1';
                    f = $(input_id).parent().parent().get(0);
                } else if (ftypes[index]=='date'){
                    input_id = '#mce-'+fnames[index]+'-month';
                    f = $(input_id).parent().parent().get(0);
                } else {
                    input_id = '#mce-'+fnames[index];
                    f = $().parent(input_id).get(0);
                }
                if (f){
                    $(f).append(html);
                    $(input_id).focus();
                } else {
                    $('#mce-'+resp.result+'-response').show();
                    $('#mce-'+resp.result+'-response').html(msg);
                }
            }
        } catch(e){
            $('#mce-'+resp.result+'-response').show();
            $('#mce-'+resp.result+'-response').html(msg);
        }
    }
}

</script>
<!--End mc_embed_signup-->

       <div class="clearfloat"></div>
       		<a href="http://charlestonhospitalitygroup.com/application" title="Apply Here" target="_blank"> <div class="gc-button">Apply Here</div></a>
       <div class="divider"></div>
       <div class="footer-icon"></div>
  
  <!--end content--> </div>
    <!-- end .footer --></div>
  <!-- end .container --></div>
  
</body>
</html>
