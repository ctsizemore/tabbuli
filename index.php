<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Tabbuli | 843.628.5959 | Authentic Mediterranean &amp; American Cuisine | Hookah Bar | Charleston, SC</title>

<meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="google-site-verification" content="" />
        <meta name="description" content="Tabbuli is Charleston's authentic Mediterranean &amp; American restaurant located just across from the Charleston Cruise Terminal." />
        <meta name="keywords" content="Tabbuli Delivers, Charleston Hookah Bar, Downtown Charleston, Mediterranean, Falafel, Hummus, Salads, Healthy, Tabouli, Pitas, Breakfast, Burgers, Outdoor Patio, outdoor seating, cruise, tapas" />
        
        <meta name="author" content="Charleston Hospitality Group" />
        <meta name="robots" content="index, follow"/>
        
		<meta itemprop="name" content="Tabbuli">
		<meta itemprop="description" content="Tabbuli is Charleston's authentic Mediterranean &amp; American restaurant located just across from the Charleston Cruise Terminal.">
		<meta itemprop="image" content="http://tabbuli.com/images/logo-color.png"> 
		
		<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@tabbuli">
<meta name="twitter:creator" content="@CHGcharleston">
<meta name="twitter:title" content="Tabbuli | Authentic Mediterranean &amp; American Cuisine | Hookah Bar">
<meta name="twitter:description" content="Tabbuli is Charleston's authentic Mediterranean &amp; American restaurant located just across from the Charleston Cruise Terminal.">
<meta name="twitter:image" content="http://tabbuli.com/images/logo-color.png">

        <link rel="shortcut icon" href="favicon.ico"> 
<link href="css/global.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Questrial|Coustard' rel='stylesheet' type='text/css'>

   <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24705264-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
	
	<style type="text/css">
		.rest-week {
			background-color: #78833A;
			letter-spacing: 1px;
			text-align: center;
			padding: 5px 0;
		}
		
		.rest-week a:link {
			color: #FFF;
		}
	</style>
 
   
   <style type="text/css">
@-moz-document url-prefix() {
    .videoPlayer {
    /*min-height: 100%;*/
max-width:300px;
/*width: 300px;*/
height: auto;
position: relative;
top:-200px;
left:0;
}

@media screen and (max-width: 700px) {
	@-moz-document url-prefix() {
    .videoPlayer {
    /*min-height: 100%;*/
max-width:300px;
/*width: 300px;*/
height: auto;
position: relative;
top:0;
left:0;
margin: 0;
}

}
</style>

    <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '896758270739348');

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=896758270739348&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->

</head>

<body>
	<div id="hover"></div>
  <div id="popup">
    <div id="close">X</div>
      <h1>Indoor, Patio and Takeout all open!</h1>
      <p>View our <a href="http://tabbuli.com/images/tabbuli-menu.pdf" title="To Go Menu" target="_blank">To Go Menu</a> or Get it delivered with Door Dash!</p><br />
      <a href="https://www.doordash.com/business/196688/?utm_source=partner-link&utm_medium=website&utm_campaign=196688&utm_content=red-l" target="_blank" alt="Order Food Delivery with DoorDash" title="Order Food Delivery with DoorDash" style="text-decoration: none"><div style="position: relative; width:289px; height:59px; margin: 0px auto; background-image: url(https://cdn.doordash.com/media/button/button_red_l.svg); color:transparent">Order Food Delivery with DoorDash</div></a>          </div>

<div id="home"></div>
 <!-- Preloader -->
<!--<div id="preloader">
	<div id="status">&nbsp;</div>
</div>-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=382880455061904";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!--<div id="nav-follow">
<div class="content">
<img src="images/tabbuli-white-leaves.png" width="30" height="30" alt="Tabbuli Leaves"> <a href="https://facebook.chownow.com/search/locationPicker?company_id=599"><span class="white-text">order tabbuli online</span></a>
<span class="white-text"> &nbsp;&nbsp;&nbsp;  • &nbsp;&nbsp;&nbsp;  843.628.5959  &nbsp;&nbsp;&nbsp;  • &nbsp;&nbsp;&nbsp; 541 King Street street charleston, sc 29401</span>
<a href="#home" title="Back to Top"><img class="fltrt" src="images/arrow.png" width="30" height="30" alt="Back to Top"></a>
<div class="clearfloat"></div>-->
<!--end content--> <!--</div>-->
<!--end nav-follow--> <!--</div>-->

<div class="container">
	
  <div class="header">
       <nav class="content">
       <div class="header-contact">
       <a href="index.php" title="Home"><img src="images/logo.png" width="210" height="80" alt="Tabbuli"></a><br />
      <h4 class="white-text phone-spacing"><a class="phone" href="tel:8436285959">Downtown &bull; 843.628.5959</a><br />
      
      </h4>
       <div class="clearfloat"></div>
       </div>
 
<ul class="nav">
        <li class="nav"><a href="index.php" title="Home">home</a></li>
        <li class="nav"><a href="menu.html" title="Lunch and Dinner Menu">menu</a></li>
        <li class="nav"><a href="brunch.html" title="Brunch Menu">brunch menu</a></li>
        </ul>
        <ul class="nav">
        <li class="nav"><a href="gallery.html" title="Gallery">gallery</a></li>
         <li class="nav"><a href="https://charlestonhospitalitygroup.securetree.com/" title="Gift Cards">gift cards</a></li>
         <li class="nav"><a href="newsletter.html" title="Sign up for our Newsletter">newsletter sign up</a></li>
        </ul>
        <ul class="nav">
	    <li class="nav"><a href="https://recruiting.paylocity.com/recruiting/jobs/All/7c844750-f56f-4030-b990-3fece9783f51/CHG-LLC" title="Careers" target="_blank">careers</a></li>
        <li class="nav"><a href="events.html" title="Blog &amp; Events">vip events</a></li>
        <li class="nav"><a href="shop.html" title="Shop Online">shop</a></li>
        </ul>
        <ul class="nav">
	        <li class="nav"><a href="https://www.ubereats.com/charleston/food-delivery/queology-and-tabbuli/aDS5AaY0RfWdiIdj3OvDcw" title="Takeout and Delivery" target="_blank">takeout + delivery</a></li>
	        <li class="nav"><a href="contact.html" title="Contact us">location + contact</a></li>
	        <li class="nav"><a href="catering.html" title="Catering">catering</a></li>
</ul> 
       <!--end content--> </nav>
    <!-- end .header --></div>
   <!--<div class="rest-week"><a href="images/restaurant-week-menu.pdf" title="2016 Restaurant Week Menu" target="_blank">Click Here to view our Restaurant Week Menu</a></div>--> 
  
   
<ul class="bxslider">
	<li><img src="images/door-dash.jpg" alt="Door Dash"><h3 class="text-center"><strong>Now on Door Dash!</strong>
</h3></li>
  <li><img src="images/tabboo-tuesday-2.jpg" alt="Tabboo Tuesday"></li>
  <li><img src="images/new-location-2.jpg" alt="New Location"></li>
  <li><img src="images/home-slider/kofta-burger.png" alt="Kofta Burger"><h3 class="text-center">Kofta Burger</h3></li>
  <li><img src="images/home-slider/classic-gyro.png" alt="Classic Gyro"><h3 class="text-center">Classic Gyro</h3></li>
</ul>
 
   <div class="box-gradient"></div>
  <div class="content">
	    <div id="circles">
<div class="center-circle"><h1 class="center-text white-text">Peace. Love. Tabbuli.</h1>

<p class="center-text white-text contact-info">
<span class="address"><i>541 King Street, Charleston, SC 29401</i></span><br /><br />
HOURS: <br /> <br />
Monday - Friday: 11 am - 2 am<br />
Saturday &amp; Sunday: 11 am - 2 am <br /><br />
<br />



<!--DELIVERY HOURS: <br /><br />

843.628.5959<br />
Monday - Friday: 11am - 3pm-->


</p>

</div>

<div class="circle-wrapper">
   <div class="circle circle-left">
   <blockquote class="circle-content white-text">"Best Bloody Mary <br />in Charleston" <br/><span class="cite-source">- Charleston Magazine</span></blockquote>
  <a href="http://charlestonmag.com/features/proud_marys" title="Learn More" target="_blank"> <div class="button">Learn More</div></a>
   </div>

   <!--end circle-wrapper--> </div>
   <div class="circle-wrapper">
   <div class="circle circle-right"><blockquote class="circle-content-2 white-text">"Amazing Sunday <br />Brunch" <br/><span class="cite-source">- Tripadvisor Reviewer</span></blockquote>
  <a href="brunch.html" title="View Menu" target="_blank"> <div class="button-2">View Menu</div></a></div>

   <!--end circle-wrapper--> </div>
  <!-- end circles--> </div>
  <div id="hr"></div>
   <h1>Do you Tabbuli?</h1>
   
   <p class="three-col">Located on the historic 541 King Street in Charleston, South Carolina; Tabbuli is a highly-rated restaurant that brings Mediterranean flair to the southern seaside. With varied brunch, lunch, and dinner menus, this restaurant and hookah lounge serves as a great spot for friends to relax and enjoy conversation over a delicious meal. While there are many single entrees to be enjoyed for the diner with a refined palate, this hot spot also features tapas-style dining options that are perfect for sharing.</p>
   
  <p class="three-col">With a health-conscious menu and a fully-stocked bar with delicious drink specials, this restaurant continues to be a favorite for both family and late-night crowds in the Charleston area. Tabbuli is Charleston's exclusive hookah bar, late-night hangout &amp; outdoor sunday brunch spot.</p>
    
   <p class="three-col"><img src="images/chicken-salad.png" width="285" height="330" class="image-padding" alt="Tabbuli Chicken Salad"></p>
  <!--end content--></div>
  <div class="bg-leaves">
  <div class="radial-shadow-top"><!--end radial-shadow-top--> </div>
  <div class="content callout">
  <h1>Eco-friendly is the new hip</h1>
  <p>Tabbul is dedicated to minimalizing our impact on the environment.  Here's how:</p>
   <div class="three-col"><img class="icon" src="images/oil-icon.png" width="120" height="120" alt="Compost"><br />
  <h3>FUEL</h3><p>We recycle all of our oil for alternate fuel sources.</p></div>
  <div class="three-col"><img class="icon" src="images/recycle-icon.png" width="120" height="120" alt="Recycle"><br />
  <h3>RECYCLE</h3><p>We recycle all aluminum, plastic &amp; paper.</p></div>
  <div class="three-col"><img class="icon" src="images/compost-icon.png" width="120" height="120" alt="Compost"><br />
  <h3>COMPOST</h3><p>We compost all leftover food.</p></div>
  
  <div class="three-col"><img class="icon" src="images/cart-icon.png" width="120" height="120" alt="Delivery"><br />
  <h3>WE DELIVER</h3><p>We cut down on emissions by delivering our delicious food on a golf cart.</p></div>
  
  <div class="three-col"><img class="icon" src="images/togo-icon.png" width="120" height="120" alt="To Go"><br />
  <h3>ECO-TO-GO</h3><p>All of our to-go boxes are 100% recyclable &amp; all of our to-go cups are 100% compostable.</p></div>
  
   <div class="three-col"><img class="icon" src="images/garden-icon.png" width="120" height="120" alt="Garden"><br />
  <h3>FLOWER POWER</h3><p>Our outdoor patio is covered in colorful plants.  It's just our way of giving back to beautiful Charleston!</p></div>
  
  </div>
  <!--end bg-leaves--> </div>
  <div class="8"></div>
  <div class="content">

  <div class="two-col">
 <!--Facebookfeed-->
                <div class="fb-page" 
                	data-href="https://www.facebook.com/tabbuli" 
                	data-tabs="timeline" data-width="500" data-height="700" data-small-header="true" 
                	data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false">
                <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/tabbuli"><a href="https://www.facebook.com/tabbuli">Tabbuli</a></blockquote>
                </div>
                </div>
           </div>   
           
           <div class="two-col">
  <h2>@tabbuli on instagram</h2>
  <div class="instagram"  ></div>
 </div>
  
  
 
  
  <!--end content--> </div>
  
   <div class="clearfloat"></div>
  <div class="footer">
  <div class="content">
  <ul class="nav footer-nav">
        <li class="nav"><a href="index.php" title="Home">home</a></li>
        <li class="nav"><a href="menu.html" title="Lunch and Dinner Menu">lunch + dinner menu</a></li>
        <li class="nav"><a href="brunch.html" title="Brunch Menu">brunch menu</a></li>
        </ul>
        <ul class="nav footer-nav">
        <li class="nav"><a href="gallery.html" title="Gallery">gallery</a></li>
        <li class="nav"><a href="http://tabbuligrill.wordpress.com/" title="Blog &amp; Events" target="_blank">blog</a></li>
        <li class="nav"><a href="newsletter.html" title="Sign up for our Newsletter">newsletter sign up</a></li>
        </ul>
        <ul class="nav footer-nav">
	    <li class="nav"><a href="events.html" title="VIP Events">vip events</a></li>
		<li class="nav"><a href="https://charlestonhospitalitygroup.securetree.com/" title="Gift Cards">gift cards</a></li>
        <li class="nav"><a href="contact.html" title="Contact us">location + contact</a></li>
  </ul>
       
        <!-- Begin MailChimp Signup Form -->

<div class="inline form-padding">
	<span class="form-label">Get Social with us</span> <br /><br />
<div id="TA_socialButtonReviews56" class="TA_socialButtonReviews">
<ul id="W6Fmi1" class="TA_links Lx1HbFsa">
<li id="R5iWGvkWYXK" class="yp4B17jpN8">
<a target="_blank" href="http://www.tripadvisor.com/Restaurant_Review-g54171-d1903985-Reviews-Tabbuli_Grill_Mediterranean_American-Charleston_South_Carolina.html"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/socialWidget/20x28_white-21692-2.png" alt="Tripadvisor"/></a>
</li>
</ul>
</div>
<script src="http://www.jscache.com/wejs?wtype=socialButtonReviews&amp;uniq=56&amp;locationId=1903985&amp;color=white&amp;size=rect&amp;lang=en_US&amp;display_version=2"></script>

<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="200" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
<br />
<a href="https://twitter.com/tabbuli" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @tabbuli</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>
<!-- Please call pinit.js only once per page -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<!--<div id="newsletter">
<form action="http://tabbuli.us6.list-manage2.com/subscribe/post?u=c752e994be9361b33f9f52d53&amp;id=e4880114ed" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>


<div class="mc-field-group">
	<label for="mce-EMAIL"><span class="form-label">Sign up for our newsletter</span></label><br />
	<input type="email" value="" name="EMAIL" class="FieldsInput" id="mce-EMAIL" placeholder="Email Address" required>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response"></div>
		<div class="response" id="mce-success-response"></div>
	</div>	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="submit-button"></div>
</form>
<!--end newsletter--> <!-- </div> -->
<!--end inline--> </div>

<div class="inline form-padding">
<div id="TA_certificateOfExcellence279" class="TA_certificateOfExcellence">
<ul id="P5EpJm2fS4L" class="TA_links q31YZu">
<li id="Tftyo1" class="R8pcCH">
<a target="_blank" href="https://www.tripadvisor.com/Restaurant_Review-g54171-d1903985-Reviews-Tabbuli_Grill_Mediterranean_American-Charleston_South_Carolina.html"><img src="https://www.tripadvisor.com/img/cdsi/img2/awards/CoE2016_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a>
</li>
</ul>
</div>
<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=279&amp;locationId=1903985&amp;lang=en_US&amp;year=2016&amp;display_version=2"></script>	
<!--end inline--> </div>

<script type="text/javascript">
var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
try {
    var jqueryLoaded=jQuery;
    jqueryLoaded=true;
} catch(err) {
    var jqueryLoaded=false;
}
var head= document.getElementsByTagName('head')[0];
if (!jqueryLoaded) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
    head.appendChild(script);
    if (script.readyState && script.onload!==null){
        script.onreadystatechange= function () {
              if (this.readyState == 'complete') mce_preload_check();
        }    
    }
}
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
head.appendChild(script);
var err_style = '';
try{
    err_style = mc_custom_error_style;
} catch(e){
    err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
}
var head= document.getElementsByTagName('head')[0];
var style= document.createElement('style');
style.type= 'text/css';
if (style.styleSheet) {
  style.styleSheet.cssText = err_style;
} else {
  style.appendChild(document.createTextNode(err_style));
}
head.appendChild(style);
setTimeout('mce_preload_check();', 250);

var mce_preload_checks = 0;
function mce_preload_check(){
    if (mce_preload_checks>40) return;
    mce_preload_checks++;
    try {
        var jqueryLoaded=jQuery;
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    try {
        var validatorLoaded=jQuery("#fake-form").validate({});
    } catch(err) {
        setTimeout('mce_preload_check();', 250);
        return;
    }
    mce_init_form();
}
function mce_init_form(){
    jQuery(document).ready( function($) {
      var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
      var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
      $("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
      options = { url: 'http://tabbuli.us6.list-manage2.com/subscribe/post-json?u=c752e994be9361b33f9f52d53&id=e4880114ed&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
                    beforeSubmit: function(){
                        $('#mce_tmp_error_msg').remove();
                        $('.datefield','#mc_embed_signup').each(
                            function(){
                                var txt = 'filled';
                                var fields = new Array();
                                var i = 0;
                                $(':text', this).each(
                                    function(){
                                        fields[i] = this;
                                        i++;
                                    });
                                $(':hidden', this).each(
                                    function(){
                                        var bday = false;
                                        if (fields.length == 2){
                                            bday = true;
                                            fields[2] = {'value':1970};//trick birthdays into having years
                                        }
                                    	if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
                                    		this.value = '';
									    } else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
                                    		this.value = '';
									    } else {
									        if (/\[day\]/.test(fields[0].name)){
    	                                        this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;									        
									        } else {
    	                                        this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
	                                        }
	                                    }
                                    });
                            });
                        return mce_validator.form();
                    }, 
                    success: mce_success_cb
                };
      $('#mc-embedded-subscribe-form').ajaxForm(options);
      
      
    });
}
function mce_success_cb(resp){
    $('#mce-success-response').hide();
    $('#mce-error-response').hide();
    if (resp.result=="success"){
        $('#mce-'+resp.result+'-response').show();
        $('#mce-'+resp.result+'-response').html(resp.msg);
        $('#mc-embedded-subscribe-form').each(function(){
            this.reset();
    	});
    } else {
        var index = -1;
        var msg;
        try {
            var parts = resp.msg.split(' - ',2);
            if (parts[1]==undefined){
                msg = resp.msg;
            } else {
                i = parseInt(parts[0]);
                if (i.toString() == parts[0]){
                    index = parts[0];
                    msg = parts[1];
                } else {
                    index = -1;
                    msg = resp.msg;
                }
            }
        } catch(e){
            index = -1;
            msg = resp.msg;
        }
        try{
            if (index== -1){
                $('#mce-'+resp.result+'-response').show();
                $('#mce-'+resp.result+'-response').html(msg);            
            } else {
                err_id = 'mce_tmp_error_msg';
                html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
                
                var input_id = '#mc_embed_signup';
                var f = $(input_id);
                if (ftypes[index]=='address'){
                    input_id = '#mce-'+fnames[index]+'-addr1';
                    f = $(input_id).parent().parent().get(0);
                } else if (ftypes[index]=='date'){
                    input_id = '#mce-'+fnames[index]+'-month';
                    f = $(input_id).parent().parent().get(0);
                } else {
                    input_id = '#mce-'+fnames[index];
                    f = $().parent(input_id).get(0);
                }
                if (f){
                    $(f).append(html);
                    $(input_id).focus();
                } else {
                    $('#mce-'+resp.result+'-response').show();
                    $('#mce-'+resp.result+'-response').html(msg);
                }
            }
        } catch(e){
            $('#mce-'+resp.result+'-response').show();
            $('#mce-'+resp.result+'-response').html(msg);
        }
    }
}

</script>
<!--End mc_embed_signup-->


  
  <!--end content--> </div>
    <!-- end .footer --></div>
  <!-- end .container --></div>
    <div id="basement">
	<div class="pineapple-wrapper">
		<div class="pineapple"></div><!-- ./pineapple -->
	</div><!-- ./pineapple-wrapper -->
	<a href="http://charlestonhospitalitygroup.com" title="Charleston Hospitality Group" target="_blank"><p>Charleston Hospitality Group</p></a>
	<div class="content-wrapper">
		<div class="fltlft">
	<a href="http://toastofcharleston.com" title="Toast!" target="_blank"><img src="images/logos/toast.png" alt="toast" width="80" height="80"></a>
	<a href="http://charlestonhospitalitycatering.com" title="Charleston Hospitality Catering" target="_blank"><img src="images/logos/catering.png" alt="catering" width="80" height="80"></a>
	<a href="http://elistable.com" title="Eli's Table" target="_blank"><img src="images/logos/elis.png" alt="elis" width="80" height="80"></a>
	</div> <!-- ./fltlft -->
	<div class="fltrght">
	<a href="http://queology.com" title="Queology BBQ" target="_blank"><img src="images/logos/queology.png" alt="queology" width="80" height="80"></a>
	<a href="http://tabbuli.com" title="Tabbuli" target="_blank"><img src="images/logos/tabbuli.png" alt="tabbuli" width="80" height="80"></a>
	<a href="http://honkytonksaloon.com" title="Honky Tonk Saloon" target="_blank"><img src="images/logos/hts.png" alt="Honky Tonk Saloon" width="80" height="80"></a>
		</div><!-- ./fltrt -->
	</div><!-- ./content-wrapper -->
	<div class="clearfix"></div>
	
</div><!-- ./basement -->
<div class="ecard"><a href="https://dianas.alohaenterprise.com/memberlink/GiftCards.html?companyID=dia06" title="Check E-Card Balance" target="_blank">Check E-Card Balance</a></div>
    <!-- Preloader -->
<script type="text/javascript">
	//<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
			$('body').delay(350).css({'overflow':'visible'});
		})
	//]]>
</script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
	    $(document).ready(function(){
  
  //chiusura al click sulla parte scura
  $("#hover").click(function(){
		$(this).fadeOut();
    $("#popup").fadeOut();
	});
  
  //chiusura al click sul pulsante
  $("#close").click(function(){
		$("#hover").fadeOut();
    $("#popup").fadeOut();
	});
  
});
    </script>
    
       <?
$user_id = "314969331"; //userid
$num_to_display = "8"; //instagram limits to max 20, but you can do less for your layout.
$access_token = "314969331.5b9e1e6.864a96dcd49440fb9e25adfdda615888"; 
?>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- begin slider scripts -->

<!-- jQuery library (served from Google) -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="js/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="css/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript">
	$(document).ready(function(){
  $('.bxslider').bxSlider();
});
	</script>

<script>

// small = + data.data[i].images.thumbnail.url +
// resolution: low_resolution, thumbnail, standard_resolution

$(function() {
    $.ajax({
    	type: "GET",
        dataType: "jsonp",
        cache: false,
        url: "https://api.instagram.com/v1/users/<?=$user_id?>/media/recent/?access_token=<?=$access_token?>",
        success: function(data) {
            for (var i = 0; i < <?=$num_to_display?>; i++) {
        $(".instagram").append("<div class='instagram-placeholder'><a target='_blank' href='" + data.data[i].link +"'><img class='instagram-image' src='" + data.data[i].images.low_resolution.url +"' /></a></div>");   
      		}     
                            
        }
    });
});

</script>

<!--begin easing script-->

	<script>
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
</body>
</html>